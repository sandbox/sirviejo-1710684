<?php

/**
 * @file
 * Displays the default view for help page
 *
 * available variables:
 * $field_group: link for field_group module page
 */
?>
<h3><?php print t('About Fields Group List') ?></h3>
<p><?php print t("This module extends !field_group and adds the posibility to create ordered and unordered lists with your grouped fields",
  array('!field_group' => $field_group)) ?></p>
