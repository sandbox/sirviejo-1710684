GENERAL DESCRIPTION
-------------------

Fieldgroup List adds an ordered or unordered list as an html wrapper for the 
FieldGroup module.
It will wrap all the elements inside this group with an <ul> or <ol> and it will
render all the elements inside the group inside a <li> element.


HOW TO USE THIS MODULE
----------------------

1)  Go to admin/structure/types and edit your content type
2)  Select Manage Display settins and in the group settings change the Field 
group format to list
    By default the list will render an unordered list, you can change it to 
render an ordered list. Note that you can add additional classes in the Extra 
CSS classes field

ISSUES
______

A great way to make this module better is to report any issue / defect you find
at http://drupal.org/project/issues/1710684

SUGGESTIONS & FEEDBACK
----------------------
I will love to read any suggestion or feedback you have for this module. 
I created for a specific need and i will enjoy adding more useful features.


CONTACTME
_________
If you want to contact me just send me an email at sirdrupal@gmail.com
