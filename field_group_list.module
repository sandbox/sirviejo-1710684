<?php

/**
 * @file
 * adds a formatter for ordered and unordered lists
 */

/**
 * Implements hook_help().
 */
function field_group_list_help($path, $arg) {
  if ($path == 'admin/help#field_group_list') {
    $field_group = l(t('Field Group'), 'http://drupal.org/project/field_group');
    return theme('field_group_list_help_text', array('field_group' => $field_group));
  }
}

/**
 * Implements hook_theme().
 */
function field_group_list_theme() {
  return array(
    'field_group_list_group_wrapper' => array(
      'variables' => array('element' => NULL, 'group' => NULL),
    ),
    'field_group_list_field_wrapper' => array(
      'variables' => array('group' => NULL, 'field_name' => NULL),
    ),
    'field_group_list_help_text' => array(
      'variables' => array('field_group' => NULL),
      'template' => 'field_group_list_help',
    ),
  );
}

/**
 * Adds the list wrapper to the selected group.
 *
 * @param array $variables
 *   the array that contains all theme variables
 */
function theme_field_group_list_group_wrapper($variables) {
  $group = $variables['group'];
  $element = $variables['element'];
  $element['#type'] = 'markup';
  $element['#weight'] = $group->weight;
  $classes = check_plain($group->format_settings['instance_settings']['classes']);
  if ($group->format_settings['formatter'] == 'ordered') {
    $element['#prefix'] = '<ol class="field-group-format ' . $group->group_name . ' ' . $classes . '">';
    $element['#suffix'] = '</ol>';
  }
  else {
    $element['#prefix'] = '<ul class="field-group-format ' . $group->group_name . ' ' . $classes . '">';
    $element['#suffix'] = '</ul>';
  }
  foreach (element_children($element) as $field_name) {
    $element[$field_name] += theme('field_group_list_field_wrapper', array('group' => $group, 'field_name' => $field_name));
  }
  return $element;
}

/**
 * Adds the <li> wrapper to each element inside the group.
 *
 * @param array $variables
 *   the array that contains all theme variables
 */
function theme_field_group_list_field_wrapper($variables) {
  $group = $variables['group'];
  $field_name = $variables['field_name'];
  $element = array();
  $element['#prefix'] = '<li class="' . $group->group_name . ' ' . $field_name . '">';
  $element['#suffix'] = '</li>';
  return $element;
}

/**
 * Implements hook_field_group_formatter_info().
 */
function field_group_list_field_group_formatter_info() {
  return array(
    'display' => array(
      'list' => array(
        'label' => t('List'),
        'description' => t('This list renders the inner content in a ordered or unordered list.'),
        'format_types' => array('ordered', 'unordered'),
        'instance_settings' => array('classes' => ''),
        'default_formatter' => 'unordered',
      ),
    ),
  );
}

/**
 * Implements hook_field_group_pre_render().
 *
 * @param Array $element
 *   the group element
 * @param Object $group
 *   The Field group info.
 */
function field_group_list_field_group_pre_render(&$element, $group, &$form) {
  if ($group->format_type === 'list') {
    $element = theme('field_group_list_group_wrapper', array('element' => $element, 'group' => $group));
  }
}
